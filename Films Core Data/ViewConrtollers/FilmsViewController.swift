//
//  ViewController.swift
//  Films Core Data
//
//  Created by Алексей on 23/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import CoreData

protocol FilmsViewControllerOutput: class {
    func filmsViewControllerDidTapAdd()
    func filmsViewControllerDidTapRemove()
}

class FilmsViewController: UIViewController {
    var output: FilmsViewControllerOutput?
    var context: NSManagedObjectContext!

    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate var addButton: UIBarButtonItem!
    fileprivate var removeButton: UIBarButtonItem!
    
/***
     frc.delegate при больших объемах данных NSFetchedResultsController забирает достаточно много Main Thread на мердж ячеек в таблице, что может фризить сам Main Thread и следовательно UI на огромный критичный промежуток времени - от нескольких секунд до десятков секунд, что негативно сказывается на пользовательсом опыте, эффективнее reloadData()
 */
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>?{
        didSet {
            do {
                if let frc = fetchedResultsController {
                    try frc.performFetch()
                    tableView.reloadData()
                }
            } catch let error {
                print("NSFetchedResultsController.performFetch() failed: \(error)")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureFRC()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleContextSaved(_:)),
                                               name: .NSManagedObjectContextDidSave,
                                               object: nil)        
    }
    
    @objc
    fileprivate func didTapAdd(_ button: UIBarButtonItem){
        isEnabledButton(false)
        output?.filmsViewControllerDidTapAdd()
    }
    
    @objc
    fileprivate func didTapRemove(_ button: UIBarButtonItem){
        isEnabledButton(false)
        output?.filmsViewControllerDidTapRemove()
    }
    
    fileprivate func isEnabledButton(_ isEnabled: Bool){
        addButton.isEnabled = isEnabled
        removeButton.isEnabled = isEnabled
    }
    
/***
     наиболее эффективный способ для импорта огромного колличества новых строк в базу данных - использовать схему конктекстов - приватный контексты для записи с непосредственным доступом к NSPersistentStoreCoordinator и мердж между приватный контекст -> main контекств для своевременного вывода новых данных на экран, но слишком частый мердж main контекста будет так же забирать весомый временной промежуток для Main Thread, по-этому для main контекста вывода на экран лучше сделать один мердж со всеми данными и вызвать reloadData()
 */
    @objc
    func handleContextSaved(_ notification: Notification) {
        let sender = notification.object as! NSManagedObjectContext
        if sender.concurrencyType == .privateQueueConcurrencyType{
            sender.perform { [weak self] in
                guard let `self` = self else { return }
                self.context.mergeChanges(fromContextDidSave: notification)
                print("merged to main")
                try! self.fetchedResultsController?.performFetch()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.isEnabledButton(true)
                }
            }
        }
    }
}

//MARK: - configure UI

extension FilmsViewController{
    fileprivate func configureUI(){
        tableView.dataSource = self
        tableView.delegate = self

        addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAdd(_:)))
        removeButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(didTapRemove(_:)))
        
        navigationItem.setLeftBarButton(removeButton, animated: false)
        navigationItem.setRightBarButton(addButton, animated: false)
    }
}

//MARK: - configure NSFetchedResultsController

extension FilmsViewController{
    fileprivate func configureFRC(){
        let request: NSFetchRequest<NSFetchRequestResult> = Film.fetchRequest()
        request.fetchBatchSize = 20
        request.sortDescriptors = [NSSortDescriptor(
            key: "duration",
            ascending: false,
            selector: #selector(NSNumber.compare(_:))
            )]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
    }
}

//MARK: - UITableViewDataSource

extension FilmsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections , sections.count > 0 {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilmCell", for: indexPath)
        if let cell = cell as? FilmCell,
            let object = fetchedResultsController?.object(at: indexPath) as? Film{
                cell.configure(with: Film_PO(from: object))
        }
        return cell
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return fetchedResultsController?.sectionIndexTitles
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return fetchedResultsController?.section(forSectionIndexTitle: title, at: index) ?? 0
    }
}

//MARK: - UITableViewDelegate

extension FilmsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return FilmCell.height
    }
}
