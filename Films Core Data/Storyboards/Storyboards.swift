//
//  Storyboards.swift
//  Films Core Data
//
//  Created by Алексей on 24/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import UIKit

enum Storyboards: String{
    case Main = "Main"
}

extension UIStoryboard{
    func instantiateViewController<ViewController: UIViewController>(_ controller: ViewController.Type) -> ViewController{
        guard let viewController = self.instantiateViewController(withIdentifier: String(describing: controller.self)) as? ViewController else {
            fatalError("storyboard can't init \(controller)")
        }
        
        return viewController
    }
}
