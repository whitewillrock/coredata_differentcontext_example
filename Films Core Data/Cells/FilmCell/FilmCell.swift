//
//  FilmCell.swift
//  Films Core Data
//
//  Created by Алексей on 24/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

class FilmCell: UITableViewCell {

    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var directorLabel: UILabel!
    @IBOutlet fileprivate weak var ageLabel: UILabel!
    @IBOutlet fileprivate weak var durationLabel: UILabel!
    @IBOutlet fileprivate weak var deskriptionLabel: UILabel!
    
    static let height: CGFloat = 210
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(with film: Film_PO){
        nameLabel.text = film.name
        directorLabel.text = film.director
        ageLabel.text = film.age
        deskriptionLabel.text = film.annotation
        durationLabel.text = "\(film.duration)"
    }
}
