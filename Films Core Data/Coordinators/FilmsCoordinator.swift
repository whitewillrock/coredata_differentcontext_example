//
//  FilmsCoordinator.swift
//  Films Core Data
//
//  Created by Алексей on 24/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import UIKit

final class FilmsCoordinator: BaseCoordinator{
    private let window: UIWindow
    fileprivate let navigationController: UINavigationController
    
    init?(window: UIWindow,
          navigationController: UINavigationController = UINavigationController()) {
        self.window = window
        self.navigationController = navigationController
    }
    
    override func start() -> Void {
        let storyboard = UIStoryboard.init(name: Storyboards.Main.rawValue, bundle: nil)
        let vc = storyboard.instantiateViewController(FilmsViewController.self)
        let controller = FilmsController()
        
        vc.output = controller
        vc.context = controller.context
        
        navigationController.setViewControllers([vc], animated: true)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return
    }
}
