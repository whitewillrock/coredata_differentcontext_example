//
//  AppCoordinator.swift
//  Films Core Data
//
//  Created by Алексей on 24/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

final class AppCoordinator: BaseCoordinator{
    private let window: UIWindow
    
    init(with window: UIWindow){
        self.window = window
    }
    
    override func start() {
        let coordinator = FilmsCoordinator(window: window)
        addDependency(coordinator)
        coordinator?.start()        
    }
}
