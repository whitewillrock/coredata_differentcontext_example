//
//  CoreDataStack.swift
//  Films Core Data
//
//  Created by Алексей on 23/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import CoreData

public protocol CoreDataStack{
    var persistentContainer: NSPersistentContainer { get }
    var mainContext: NSManagedObjectContext { get }
    var temporyContext: NSManagedObjectContext { get }
    func background(task: @escaping (NSManagedObjectContext) -> Void)
}

/***
 использование только MAIN контекста работает только в главном потоке, использует максимальный промежуток времени главного потока и на длительный срок блокирует полностью пользовательский UI, использовать не рекомендуется
 MainContext <--> NSPersistentStoreCoordinator
 */

final class CoreDataStackClassic: CoreDataStack{
    var persistentContainer: NSPersistentContainer{
        return CoreDataStackStatic.shared.persistentContainer
    }
    
    var mainContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = CoreDataStackStatic.shared.persistentContainer.persistentStoreCoordinator
        return context
    }
    
    var temporyContext: NSManagedObjectContext{
        fatalError("for classic stack you can not use other private context, only main context")
    }
    
    func background(task: @escaping (NSManagedObjectContext) -> Void){
        fatalError("for classic stack you can not use other private queue, only main")
    }
}

/***
 c iOS 5 до iOS 9 перспективно использовать вложенные контексты, private context использовались для сервисов по созданию и добавленю новых записей, данные из которые мерджились в Main Context. Main context отвечает за вывод информации на экран, так же за функции inser delete update записей и связь с NSFetchResultController, при этом все накопленные изменения мерджились во Writer Context, который единственный имел доступ до NSPersistentStoreCoordinator, и отвечал только за запись данных в Store. Узким местом был сам принцип CoreData - либо произходит запись или произходит чтение данных, производительность уступает схемам ниже.
     PrivateContext (P) -> MainContext (M) -> WriterContext (P) <--> NSPersistentStoreCoordinator
 */

final class CoreDataStackInheritance: CoreDataStack{
    var persistentContainer: NSPersistentContainer{
        return CoreDataStackStatic.shared.persistentContainer
    }
    
    var mainContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = writerContext
        return context
    }
    
    var temporyContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.parent = mainContext
        return context
    }
    
    var writerContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = CoreDataStackStatic.shared.persistentContainer.persistentStoreCoordinator
        return context
    }
    
    func background(task: @escaping (NSManagedObjectContext) -> Void){
        CoreDataStackStatic.shared.persistentContainer.performBackgroundTask(task)
    }
}

/***
 Нативный Core Data стек, используется с IOS 10, отлично показывает в большинстве случаев. Строится по следующей схеме - каждый main context имеет непосредственный доступ к NSPersistentStoreCoordinator, может совершать процедуру мерджа с другими контекстами по средствам свойства automaticallyMergesChangesFromParent = true, какждый private context так же имеет непосредственный доступ к NSPersistentStoreCoordinator, может совершать процедуру мерджа. Основным новшеством стало добавление сущности NSPersistentContainer, которая берет на себя обязанность разруливания процессов чтение и записи в стор, т.е. одновременно может произходить 1 запись и много чтений из базы данных.
     MainContext <--> NSPersistentStoreCoordinator
     PrivateContext <--> NSPersistentStoreCoordinator
 */

final class CoreDataStackNative: CoreDataStack{
    var persistentContainer: NSPersistentContainer{
        return CoreDataStackStatic.shared.persistentContainer
    }
    
    var mainContext: NSManagedObjectContext{
        return CoreDataStackStatic.shared.persistentContainer.viewContext
    }
    
    var temporyContext: NSManagedObjectContext{
        return CoreDataStackStatic.shared.persistentContainer.newBackgroundContext()
    }
    
    func background(task: @escaping (NSManagedObjectContext) -> Void){
        CoreDataStackStatic.shared.persistentContainer.performBackgroundTask(task)
    }
}

/***
 наиболее перспективный CoreData стек. Используется принцип, описанный выше, но с ручным указанием MergePolicy и без автоматичнского мерджа являеется более гибким решением, дает возможность указать политики мерджа между контекстами и при использование регулярном ручном мердже каждых X элементом между контекстами дает наивысший перфоманс по работе с CoreData в многопоточном режиме
 */

final class CoreDataStackDefault: CoreDataStack{
    var persistentContainer: NSPersistentContainer{
        return CoreDataStackStatic.shared.persistentContainer
    }
    
    var mainContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = CoreDataStackStatic.shared.persistentContainer.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }
    
    var temporyContext: NSManagedObjectContext{
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = CoreDataStackStatic.shared.persistentContainer.persistentStoreCoordinator
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }
    
    func background(task: @escaping (NSManagedObjectContext) -> Void){
        persistentContainer.performBackgroundTask(task)
    }
}

/***
 сам cинглтон для стека базы данных является приватным, по причине - в фабриках или DI фреймворках может быть использована другая имлементация, отсуствует строгая привязка к этому паттерну
 */
private class CoreDataStackStatic{
    private let database = "Films"
    static let shared = CoreDataStackStatic()
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: database)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError?{
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func printDatabaseStatistice() {
        let context = CoreDataStackStatic.shared.persistentContainer.viewContext
        context.perform {
            let searchCount = try! context.count(for: Film.fetchRequest())
            print("\(searchCount) фильмов")
        }
    }
}

