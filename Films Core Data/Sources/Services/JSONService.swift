//
//  JSONService.swift
//  Films Core Data
//
//  Created by Алексей on 23/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

public typealias JSON = [String: Any]

public enum JSONError: Error{
    case wrongData
}

public enum Result<Element, E: Error>{
    case success(Element)
    case failure(E)
}

protocol JSONService {
    func open<Element: JSONDecodable>(item: Element.Type, handler: @escaping (Result<[Element], JSONError>) -> ())
}

private let fileName = "films"

class JSONServiceDefault: JSONService{
    func open<Element>(item: Element.Type,
                       handler: @escaping (Result<[Element], JSONError>) -> ()) where Element : JSONDecodable {
        DispatchQueue.global(qos: .background).async {
            guard let url = Bundle.main.url(forResource: fileName, withExtension: "json") else { return }
            do {
                let data = try Data(contentsOf: url)
                guard let root = try JSONSerialization
                    .jsonObject(with: data, options: .allowFragments) as? [JSON] else { throw JSONError.wrongData }
                let elements = root.flatMap{ Element(from: $0) }
                handler(.success(elements))
            }
            catch let error as JSONError{
                handler(.failure(error))
            }
            catch {
                handler(.failure(JSONError.wrongData))
            }
        }
    }
}

