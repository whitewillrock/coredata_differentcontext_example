//
//  FilmsSaver.swift
//  Films Core Data
//
//  Created by Алексей on 24/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import CoreData

protocol FilmsSaver: class {
    func save()
    func remove()
}

class FilmsSaverDefault: FilmsSaver{
    let coreStack: CoreDataStack
    let JSONService: JSONService
    
    init(
        coreStack: CoreDataStack = CoreDataStackDefault(),
        JSONService: JSONService = JSONServiceDefault()
        ) {
        self.coreStack = coreStack
        self.JSONService = JSONService
    }
    
    func save(){
        JSONService.open(item: Film_PO.self) { [weak self] (result) in
            guard let `self` = self else { return }
            switch result{
            case .success(let items):
                self.saveContext(items)
            case .failure(let error):
                print(error.localizedDescription)
                //handle error
            }
        }
    }
    
    func remove(){
        let context = coreStack.temporyContext
        let request: NSFetchRequest<NSFetchRequestResult> = Film.fetchRequest()
        context.perform {
            do{
                let items = try context.fetch(request) as? [NSManagedObject]
                _ = items.map{ $0.map{ context.delete($0) } }
                try context.save()
                context.reset()
                
            }catch{
                // handle error
            }
        }
    }
    
    /***
     пример сохранения контекста тестовый без хандинга ошибок
 */
    
    private func saveContext(_ items: [Film_PO]){
        let context = coreStack.temporyContext
        context.perform {
            for index in 0..<items.count{
                _ = Film(from: items[index], in: context)
                
//                if index % 1000 == 0{
//                    do {
//                        try context.save()
//                        context.reset()
//                    } catch {
//                        // handle error
//                    }
//                }
            }
            do {
                try context.save()
                context.reset()
            } catch {
                // handle error
            }
        }
    }
}

extension Film{
    convenience init(from film: Film_PO, in context: NSManagedObjectContext){
        self.init(context: context)
        age = film.age
        annotation = film.annotation
        director = film.director
        duration = Int32(film.duration)
        name = film.name
        production = film.production
    }
}

//        print("func save()")
//        let context = coreStack.temporyContext
//        context.perform {
//            for index in 0...30000{
//                let f = Film(context: context)
//                f.age = "film.age\(index)"
//                f.annotation = "film.annotatiozsn\(index)"
//                f.director = "film.director\(index)"
//                f.duration = Int32(index)
//                f.name = "()printDatabaseStatistice()\(index)"
//                f.production = "film.production\(index)"
//
////                if index % 1000 == 0{
////                    do {
////                        try context.save()
////                        context.reset()
////                    } catch {
////                        // handle error
////                    }
////                }
//            }
//            do {
//                try context.save()
//                context.reset()
//            } catch {
//                // handle error
//            }
//        }
//    }
//}

