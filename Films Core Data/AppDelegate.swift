//
//  AppDelegate.swift
//  Films Core Data
//
//  Created by Алексей on 23/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coordinator: BaseCoordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        coordinator = AppCoordinator(with: window!)
        coordinator.start()
                        
        return true
    }
}

extension String{
    
}

