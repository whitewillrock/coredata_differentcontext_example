//
//  Film.swift
//  Films Core Data
//
//  Created by Алексей on 23/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

protocol JSONDecodable {
    init?(from json: JSON)
}

struct Film_PO: JSONDecodable{
    var age: String
    var annotation: String
    var director: String
    var duration: Int = 0
    var name: String
    var production: String
    
    init?(from json: JSON) {
        guard let film = json["general"] as? JSON else { return nil }
        
        age = film["ageCategory"] as? String ?? ""
        annotation = film["annotation"] as? String ?? ""
        director = film["director"] as? String ?? ""
        name = film["filmname"] as? String ?? ""
        production = film["countryOfProduction"] as? String ?? ""
        duration = duration(of: film)
    }
    
    init(from film: Film) {
        age = film.age ?? ""
        annotation = film.annotation ?? ""
        director = film.director ?? ""
        duration = Int(film.duration ?? 0)
        name = film.name ?? ""
        production = film.production ?? ""
    }
    
    private func duration(of json: JSON) -> Int{
        let hours = (Int(json["durationHour"] as? String ?? "") ?? 0)
        let minutes = Int(json["durationHour"] as? String ?? "") ?? 0
        return (hours * 60) + minutes
    }
}
