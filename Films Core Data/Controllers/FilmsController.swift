//
//  FilmsController.swift
//  Films Core Data
//
//  Created by Алексей on 27/10/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import CoreData

class FilmsController{
    
    var context: NSManagedObjectContext{
        return coreStack.mainContext
    }
    
    private let filmsSaver: FilmsSaver
    private let coreStack: CoreDataStack
    
    init(
        coreStack: CoreDataStack = CoreDataStackDefault(),
        filmsSaver: FilmsSaver = FilmsSaverDefault()) {
        self.filmsSaver = filmsSaver
        self.coreStack = coreStack
    }
    
    fileprivate func save(){
        filmsSaver.save()
    }
    
    fileprivate func remove() {
        filmsSaver.remove()
    }
}

extension FilmsController: FilmsViewControllerOutput{
    func filmsViewControllerDidTapAdd() {
        save()
    }
    
    func filmsViewControllerDidTapRemove() {
        remove()
    }
}
